﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;
    public Text lasergunmessage;
    public Text minigunmessage;
    public Text healthpackmessage;
    public Text noclipmessage;
    public Text doublescoremessage;
    Vector3 movement;
    Animator anim;
    Rigidbody playerRigidbody;
    int floorMask;
    float camRayLength = 100f;
    EnemyHealth TakeDamage;
    PlayerShooting Shoot;
    float timeToRun = 10.0f;
    

    void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");

        anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        Move(h, v);

        Turning();

        Animating(h, v);
    }

    void Move(float h, float v)
    {
        movement.Set(h, 0f, v);

        movement = movement.normalized * speed * Time.deltaTime;

        playerRigidbody.MovePosition(transform.position + movement);
    }

    void Turning()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit floorHit;

        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;

            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

            playerRigidbody.MoveRotation(newRotation);
        }
    }

    void Animating(float h, float v)
    {
        bool walking = h != 0f || v != 0f;

        anim.SetBool("IsWalking", walking);
    }

    private IEnumerator PhaseBallFunctions()
    {
        playerRigidbody.detectCollisions = false;

        lasergunmessage.color = new Color (1f, 1f, 1f, 1f);
        yield return new WaitForSeconds(15);
        playerRigidbody.detectCollisions = true;
        lasergunmessage.color = new Color(1f, 1f, 1f, 0f);

    }

    private IEnumerator SpeedBoostFunctions()
    {
        //make player faster
        yield return new WaitForSeconds(20);
        //reset player speed

    }

    private IEnumerator LazerGunFunctions()
    {
        PlayerShooting.damagePerShot = 50;
        PlayerShooting.timeBetweenBullets = .5f;
        
        yield return new WaitForSeconds(15);
        PlayerShooting.damagePerShot = 20;
        PlayerShooting.timeBetweenBullets = .15f;

    }

    private IEnumerator MiniGunFunctions()
    {
        PlayerShooting.damagePerShot = 25;
        PlayerShooting.timeBetweenBullets = .1f;
        yield return new WaitForSeconds(20);
        PlayerShooting.damagePerShot = 20;
        PlayerShooting.timeBetweenBullets = .15f;

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("MiniGun"))
        {
            Destroy(other.gameObject);
            StartCoroutine(LazerGunFunctions());
        }

        if (other.gameObject.CompareTag("LaserShot"))
        {
            Destroy(other.gameObject);
            StartCoroutine(LazerGunFunctions());
        }

        if (other.gameObject.CompareTag("Multiplier"))
        {
            ScoreManager.score *= 2;

            //Add Delay for 30 seconds

            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("PhaseBall"))
        {
            Destroy(other.gameObject);
            StartCoroutine(PhaseBallFunctions());
        }

    }
}