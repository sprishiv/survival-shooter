﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public GameObject item;
    public float spawnTime = 3f;
    public Transform[] spawnPoints;



    void Start ()
    {
        InvokeRepeating ("Spawn", spawnTime, spawnTime);
    
	}
	
	void Spawn ()
    {
        if (playerHealth.currentHealth <= 0f)
        {
            return;
        }

        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        Instantiate(item, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
        //Instantiate(item, new Vector3(6.22f, 0f, -14.14f), new Quaternion(0, 0, 0, 0));
    }
}
